# certificate

A short overview of the generated parts can be found
in the [PDK documentation][1].

## Table of Contents

- [certificate](#certificate)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Setup](#setup)
    - [Setup Requirements](#setup-requirements)
    - [Beginning with certificate](#beginning-with-certificate)
  - [Development](#development)

## Description

This module installs SSL certificates with or without Let's Encrypt.

## Setup

### Setup Requirements

If Let's Encrypt certificate are used, this module requires
`puppet-letsencrypt` and it's dependencies.

### Beginning with certificate

Include `certificate` and it will find certificates in Hiera or create Let's
Encrypt certificates based on Hiera data.

## Development

In the Development section, tell other users the ground rules for contributing
to your project and how they should submit their work.
