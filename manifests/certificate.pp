# @summary Enforce an SSL certificate and key
#
# @example
#   certificate::certificate { 'a.example.com':
#     certificate => '...',
#     key         => '...',
#   }
#
# @param certificate
#   The SSL certificate
# @param key
#   The SSL private key
# @param certificate_path
#   The absolute path of the SSL certificate
# @param key_path
#   The absolute path of the SSL private key
#
define certificate::certificate (
  String[10]           $certificate,
  String[10]           $key,
  Stdlib::Absolutepath $certificate_path = "/etc/ssl/certs/${title}.crt",
  Stdlib::Absolutepath $key_path         = "/etc/ssl/private/${title}.key",
) {
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0600',
    ;
    $certificate_path:
      content => $certificate,
      mode    => '0644',
    ;
    $key_path:
      content => $key
    ;
  }
}
