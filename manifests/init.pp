# @summary Installs and configures certbot.
#
# Don't include this class, call the appropriate defined types and if required, they will include this class.
#
# @param email
#   The email address to use to register with Let's Encrypt. The
#   [`letsencrypt` email parameter](https://github.com/voxpupuli/puppet-letsencrypt/blob/master/REFERENCE.md#email)
# @param api
#   The [Let's Encrypt API](https://letsencrypt.org/docs/staging-environment/) to use.
#
class certificate(
  Optional[String[7]]           $email = undef,
  Enum['production', 'staging'] $api   = 'staging',
) {
  $server = $api ? {
    'production' => 'https://acme-v02.api.letsencrypt.org/directory',
    'staging' => 'https://acme-staging-v02.api.letsencrypt.org/directory'
  }
  class { 'letsencrypt' :
    email  => $email,
    config => {
      'server' => $server,
    },
  }
}
