# @summary Create a Let's Encrypt certificate
#
# @example
#   certificate::letsencrypt { 'a.example.com':
#     domains => ['a.example.com', 'b.example.com'],
#   }
#
# @param domains
#   An array of domains to include in the CSR.
#   [`letsencrypt` domains](https://github.com/voxpupuli/puppet-letsencrypt/blob/master/REFERENCE.md#domains)
# @param plugin
#   The authenticator plugin to use when requesting the certificate.
#   [`letsencrypt` plugin](https://github.com/voxpupuli/puppet-letsencrypt/blob/master/REFERENCE.md#plugin)
#
define certificate::letsencrypt (
  Optional[Array[String[5]]] $domains = undef,
  Optional[Enum['nginx']]    $plugin = 'nginx',
) {
  include certificate

  letsencrypt::certonly { $title:
    domains => $domains,
    plugin  => $plugin,
  }
}
