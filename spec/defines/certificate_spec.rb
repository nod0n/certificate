# frozen_string_literal: true

require 'spec_helper'

describe 'certificate::certificate' do
  let(:title) { 'namevar' }
  let(:params) do
    {
      # snakeoil
      certificate: '
        -----BEGIN CERTIFICATE-----
        MIIC5TCCAc2gAwIBAgIUdFtM9JR0O+LTaFlHFgukew91t10wDQYJKoZIhvcNAQEL
        BQAwGDEWMBQGA1UEAwwNcC1zdi1jYy1ycHgwMTAeFw0yMDA4MDQwODE1MTNaFw0z
        MDA4MDIwODE1MTNaMBgxFjAUBgNVBAMMDXAtc3YtY2MtcnB4MDEwggEiMA0GCSqG
        SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDGMV2xukesitdzlJH6u5swpP3DHYhF/RxO
        2j08DQmA7XC9mRNd5LcRIY512e84ODBq42S3LpbUPOvGdvuXCIC0j16aLKpeOhNy
        MVKaxNxK4pE2oLLf6qNAyfJzrciCZ+/dcMnMoT/Cmxud8llvsY3HyedEoqKVYnMK
        JXTSguHC66oa+nyBWLkJJbBP6pSgX7JSfeIBgx4L+hPVxNmSB1JKe6DPyz8HzJWm
        lce1BvafyysxjfpN8KdQ26jOONcXYXa/kLEI8U3RllOL+mqtProAqP5tPP3B/rVa
        R6den8A5Mw5wkr+xzoo+GEG6tTidJZZ4STVwNtfvZxDB53HDzyqtAgMBAAGjJzAl
        MAkGA1UdEwQCMAAwGAYDVR0RBBEwD4INcC1zdi1jYy1ycHgwMTANBgkqhkiG9w0B
        AQsFAAOCAQEAUpOGjcNrx/EcoLMMr9j5++v33Mxi/FONTdasvmoHMKl97lHoB7qe
        Qk+fXzixcqD5C8Xhs/mHtpFghI55eadQjehP1QYrVQCQkWnIselRl58UwieaUyHH
        Pd8RXxeaeNntwhBhem+2rDHO3DQGmwRTbqApjmDAgbEJ5PeZ1Ic0nPmn640iK+gD
        krYTw4sUTeQ9SoKzzmEzGIyQsi+CIZsUP/CTX9oUmuyR/S+DuHaGaReiNthGzZjl
        HP6elHBoCSThkTKtMI7n/y1GffUaMmN8SdGs9hzouJWrbdNyOUg276z/eTzR0M3U
        xhbgcB5FHJUTUCq+uUn1Da7P3dghO+LP2g==
        -----END CERTIFICATE-----
      ',
      key: '
        -----BEGIN PRIVATE KEY-----
        MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDGMV2xukesitdz
        lJH6u5swpP3DHYhF/RxO2j08DQmA7XC9mRNd5LcRIY512e84ODBq42S3LpbUPOvG
        dvuXCIC0j16aLKpeOhNyMVKaxNxK4pE2oLLf6qNAyfJzrciCZ+/dcMnMoT/Cmxud
        8llvsY3HyedEoqKVYnMKJXTSguHC66oa+nyBWLkJJbBP6pSgX7JSfeIBgx4L+hPV
        xNmSB1JKe6DPyz8HzJWmlce1BvafyysxjfpN8KdQ26jOONcXYXa/kLEI8U3RllOL
        +mqtProAqP5tPP3B/rVaR6den8A5Mw5wkr+xzoo+GEG6tTidJZZ4STVwNtfvZxDB
        53HDzyqtAgMBAAECggEBAJ9JjcQJsxE5hW+fOLQLk+7ErC7kzWFyzA2T2SMqVzkH
        nFTFcotEBt392kFdpqXIgyFlIM0Y5KAvVk9r0UjRzJ9RmvbObx7MDlT4zysUjPii
        n5MIFLtiGVM4+2N9iGlGn8GZpJaSAmylvMx0uuzKBulLnFPNYl4tgcfl0r5d8pR0
        6WogkkAeMTZQzESvVtw2o0PVWYgMU6dZ3H4l8voBt6DNuyFWc+1rZoQjqiENf6TG
        q5eRpOEkKr9H4Buj4Ge7IjncI4e9kWhK/5DvaLQMFCLp49p8Qwi5Uf792Nv3DQUo
        2QN2Vvko5PO5jXJqXU+1Q6RlNtdfM1I5WFojYSE02AECgYEA49DhzYsp9ynwXGSE
        ce9mjqx78Y+tkswTkRiuLOWUQriqebE4sLYfrz2lzyqtj7Y32nTahgw+fjXiGgdg
        WzA2+GUWg9DB3CMsZpZcrXvPcOpxHnZaGV9x9dYxpC6Sqv/R+5pPQQy5Jrgoq4tm
        K0yUj2/Alno2g/XqLMRYmqxBUCkCgYEA3rZLtuPn6FfjgdoASTTLujobTZ5xP7j9
        0kCnY2t+VCU7ZgkGKnNtqJbsoqptxdn97ayKGc2grv+qtSzaXptx/QHVfA24f36v
        GP5olEW1O8tDWeClKMsVLBy87yeTQqgqLk5Bz5mTOHVNNYscRfS24eSQQ9Z6Nd9z
        +VvUOpulhuUCgYAAhXve0pC4BhqMu5YqBJhBf1eRR/jnuxvYWywa4jOiq3WIbcVp
        ciFEb2mvvbND/vDFyTquz9xcwZbQ5G9uBgA6NVEF4DZxYIs7eOChbQUehwrXPQIb
        yMIkA+d9LrYbOhaq8sF3rqXF1uoQ6nLDapt0cJcKQSFaz/dHPvPM9tP3cQKBgFHz
        2uCt9MR2su0xfzFxEBazEyD40MuWEzCvQek/wPjNypG9eFtepleHUgR5mnNXyC0R
        dLASDrLWucZ5ftGZedSUPC6uDdo8j8ous+T70bjzDxGfrKbaNCxLVuraMUIyB3OK
        sIToCliYMZUmX30Ok82EAeI0pjv0wgRcWjhG1h5RAoGACtItxBr0US9v+4iLrUQZ
        bYLBoBEpDO74q1S8YDwp24s+T8mse33HQ2B37YmeeClsvgJu60TFrDZRx96XrjXP
        0s2z28jnUo/AkD/VlOhjZmD8lARAIUurheRr5XwxmyWvzIRsnQOIDSfy1GXqant3
        wlvFh3wG+IkYvfVumUPBWfU=
        -----END PRIVATE KEY-----
      '
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
