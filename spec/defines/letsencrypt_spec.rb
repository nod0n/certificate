# frozen_string_literal: true

require 'spec_helper'

describe 'certificate::letsencrypt' do
  let(:pre_condition) do
    "class { certificate:
      email => 'letsregister@example.com'
    }"
  end
  let(:title) { 'namevar' }
  let(:params) do
    { domains: ['a.example.com', 'b.example.com'] }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
